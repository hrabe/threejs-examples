varying vec2 vUv;
varying vec3 vPos;
varying vec2 coordinates;
attribute vec3 coordinatesAttribute; 
attribute float particleSpeed;
attribute float offset;
attribute float direction;
attribute float press;

uniform float move;
uniform float time;
uniform vec2 mouse;
uniform float mousePressed;
uniform float transition;

void main() {
    vUv = uv;

    vec3 pos = position;
    //Not Stable
    pos.x += sin(move * particleSpeed) * 3.;
    pos.y += sin(move * particleSpeed) * 3.;
    pos.z = mod(position.z + move * 20. * particleSpeed + offset, 2000.) - 1000.;

    //Stable
    vec3 stable = position;
    float dist = distance(stable.xy, mouse);
    float area = 1. - smoothstep(0., 500., dist);

    stable.x += 50. * sin(time * 0.1 * press) * direction * area * mousePressed;
    stable.y += 50. * sin(time * 0.1 * press) * direction * area * mousePressed; 
    stable.z += 200. * cos(time * 0.1 * press) * direction * area * mousePressed; 

    pos = mix(pos, stable, transition);

    //Stable
    vec4 mvPosition = modelViewMatrix * vec4(pos, 1.);
    gl_PointSize = 4000. * (1. / - mvPosition.z );
    gl_Position = projectionMatrix * mvPosition;

    coordinates = coordinatesAttribute.xy;
    vPos = pos;
}