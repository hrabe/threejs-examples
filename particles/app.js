// basic template get from https://github.com/mrdoob/three.js/
// edited by https://www.youtube.com/watch?v=8K5wJeVgjrM&list=PLswdBLT9llbheHhZdGNw9RehJP1kvpMHY&index=9
import * as THREE from 'three';
import fragment from './shaders/fragment.glsl'
import vertex from './shaders/vertex.glsl'

import mask from './img/mask.jpg'
import t1 from './img/t1.jpg'
import t2 from './img/t2.jpg'

import { OrbitControls} from './node_modules/three/examples/jsm/controls/OrbitControls'
import gsap from 'gsap'
import * as dat from 'dat.gui'

export default class Sketch {
    constructor(){

        this.renderer = new THREE.WebGLRenderer( { antialias: true } );
        this.renderer.setSize( window.innerWidth, window.innerHeight );
        // this.renderer.setAnimationLoop( this.animation );
        document.getElementById('container').appendChild( this.renderer.domElement );

        this.camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.1, 3000 );
        this.camera.position.z = 1000;
        this.scene = new THREE.Scene();

        this.raycaster = new THREE.Raycaster();
        this.mouse = new THREE.Vector2();
        this.point = new THREE.Vector2();

        this.textures = [
            new THREE.TextureLoader().load(t1),
            new THREE.TextureLoader().load(t2),
        ]
        this.mask = new THREE.TextureLoader().load(mask)

        this.time = 0;
        this.move = 0;
        // this.controls = new OrbitControls(this.camera, this.renderer.domElement)
        this.settings()
        this.addMesh()

        this.mouseEffects()
        this.render()
    }

    random(a,b) {
        return a + (b - a)*Math.random()
    }

    onMouseMove( event ) {

        this.test = new THREE.Mesh(
            new THREE.PlaneBufferGeometry(2000,2000),
            new THREE.MeshBasicMaterial()
        )

        // calculate mouse position in normalized device coordinates
        // (-1 to +1) for both components
    
        this.mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        this.mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

        // update the picking ray with the camera and mouse position
	    this.raycaster.setFromCamera( this.mouse, this.camera );   

        // calculate objects intersecting the picking ray
        //const intersects = this.raycaster.intersectObjects( this.scene.children );
        const intersects = this.raycaster.intersectObjects( [this.test] );
        console.log(intersects[0].point)

        this.point.x = intersects[0].point.x
        this.point.y = intersects[0].point.y
    }

    settings(){
        let that = this;
        this.settings = {
            progress: 0,
        }
        this.gui = new dat.GUI()
        this.gui.add(this.settings, "progress", 0, 1, 0.01)
    }

    mouseEffects() {

        window.addEventListener( 'mousemove', (event) => this.onMouseMove(event), false );

        window.addEventListener('mousewheel', (e) => {
            // console.log(e.wheelDeltaY)
            this.move += e.wheelDeltaY/40
        })

        window.addEventListener('mousedown', (e) => {
            // console.log(e.wheelDeltaY)
            // this.move += e.wheelDeltaY/10
            gsap.to(this.material.uniforms.mousePressed,{
                duration: 1,
                value: 1,
                ease: "elastic.out(1, 0.3)"
            })
        })

        window.addEventListener('mouseup', (e) => {
            // console.log(e.wheelDeltaY)
            // this.move += e.wheelDeltaY/10
            gsap.to(this.material.uniforms.mousePressed,{
                duration: 1,
                value: 0,
                ease: "elastic.out(1, 0.3)"
            })
        })
    }

    addMesh() {
        this.material = new THREE.ShaderMaterial({
            fragmentShader: fragment,
            vertexShader: vertex,
            uniforms: {
                progress: { type: "f", value: 0 },
                t1: { type: "t", value: this.textures[0]},
                t2: { type: "t", value: this.textures[1]},
                mask: { type: "t", value: this.mask},
                mouse: { type: "v2", value: null},
                transition: { type: "f", value: null},
                mousePressed: { type: "f", value: 0},
                move: { type: "f", value: 0 },
                time: { type: "f", value: 0 },
            },
            side:THREE.DoubleSide,
            transparent: true,
            depthTest: false,
            depthWrite: false,
        })
        let number = 512*512
        this.geometry = new THREE.BufferGeometry()
        // this.geometry = new THREE.PlaneBufferGeometry( 1000,1000,10,10 );
        // *3 particales have 3 coordinates
        this.positions = new THREE.BufferAttribute(new Float32Array(number * 3), 3)
        this.coordinates = new THREE.BufferAttribute(new Float32Array(number * 3), 3)
        this.speeds = new THREE.BufferAttribute(new Float32Array(number), 1)
        this.offset = new THREE.BufferAttribute(new Float32Array(number), 1)
        this.direction = new THREE.BufferAttribute(new Float32Array(number), 1)
        this.press = new THREE.BufferAttribute(new Float32Array(number), 1)

        let index = 0
        for (let i = 0; i < 512; i++) {
            let posX = i - 256;
            for (let j = 0; j < 512; j++) {
                this.positions.setXYZ(index, posX*2, (j-256)*2, 0)
                this.coordinates.setXYZ(index, i, j, 0)
                this.offset.setX(index, this.random(-1000,1000))
                this.speeds.setX(index, this.random(0.4,1))
                this.direction.setX(index, Math.random() > 0.5 ? 1: -1)
                this.press.setX(index, this.random(0.4,1))
                index++;
            }
        }
        this.geometry.setAttribute("position", this.positions);
        this.geometry.setAttribute("coordinatesAttribute", this.coordinates);
        this.geometry.setAttribute("particleSpeed", this.speeds);
        this.geometry.setAttribute("offset", this.offset);
        this.geometry.setAttribute("direction", this.direction);
        this.geometry.setAttribute("press", this.press);
        // this.mesh = new THREE.Mesh( this.geometry, this.material );
        this.mesh = new THREE.Points(this.geometry, this.material)
	    this.scene.add( this.mesh );
    }

    render() {
        this.time++;
        let next = Math.floor(this.move + 40)%2
        let prev = (Math.floor(this.move) + 1 + 40)%2
        // this.mesh.rotation.x = this.time / 200;
	    // this.mesh.rotation.y = this.time / 100;
        this.material.uniforms.t1.value = this.textures[next]
        this.material.uniforms.t2.value = this.textures[prev]

        this.material.uniforms.transition.value = this.settings.progress
        this.material.uniforms.time.value = this.time
        this.material.uniforms.move.value = this.move
        this.material.uniforms.mouse.value = this.point
	    this.renderer.render( this.scene, this.camera );
        console.log(this.move);
        window.requestAnimationFrame(this.render.bind(this));
    }
}

new Sketch();
