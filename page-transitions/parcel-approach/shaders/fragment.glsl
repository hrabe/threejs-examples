uniform float time;
uniform float progress;
uniform sampler2D t1,t2;
uniform vec4 resolution;
varying vec2 vUv;
varying vec3 vPos;
float PI = 3.141592653589793238;

void main() {
    vec2 myUV = (vUv - vec2(0.5))*resolution.zw + vec2(0.5);
    vec4 image_t1 = texture2D(t1, myUV);
    vec4 image_t2 = texture2D(t2, myUV);
    float dist = distance(image_t1, image_t2)/2.;
    dist = myUV.x + 0.1 * sin(myUV.y * 10. + time);
    float pr = step(dist, progress);

    vec4 final = mix(image_t1, image_t2, pr);
    gl_FragColor = final;
    // gl_FragColor = image_t1;
}