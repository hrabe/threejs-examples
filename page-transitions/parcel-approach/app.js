// basic template get from https://github.com/mrdoob/three.js/
// edited by https://www.youtube.com/watch?v=8K5wJeVgjrM&list=PLswdBLT9llbheHhZdGNw9RehJP1kvpMHY&index=9
import * as THREE from 'three';
import { OrbitControls} from 'three/examples/jsm/controls/OrbitControls'
import fragment from './shaders/fragment.glsl'
import vertex from './shaders/vertex.glsl'

import * as dat from 'dat.gui'
import gsap from 'gsap'

import about from './img/ferns-3-1405636.jpg'
import blog from './img/yellowstone-river-1361768.jpg'

export default class Sketch {
    constructor(options){
        this.scene = new THREE.Scene();
        
        this.container = options.dom
        this.width = this.container.offsetWidth
        this.height = this.container.offsetHeight

        this.renderer = new THREE.WebGLRenderer( { antialias: true } );
        this.renderer.setPixelRatio(window.devicepixelratio)
        this.renderer.setSize( this.width, this.height );
        this.renderer.setClearColor(0xeeeeee, 1)
        this.renderer.physicallyCorrectLights = true
        this.renderer.outputEncoding = THREE.sRGBEncoding

        this.textures = [
            new THREE.TextureLoader().load(about),
            new THREE.TextureLoader().load(blog),
        ]

        this.container.appendChild(this.renderer.domElement)
        // this.renderer.setAnimationLoop( this.animation );
        // document.getElementById('container').appendChild( this.renderer.domElement );

        this.camera = new THREE.PerspectiveCamera(
            70,
            window.innerWidth / window.innerHeight,
            0.001,
            1000
        )
        
        var frustumSize = 1;
        var aspect = window.innerWidth / window.innerHeight
        this.camera = new THREE.OrthographicCamera(
            frustumSize / -2, frustumSize / 2,
            frustumSize / 2, frustumSize / -2,
             -1000, 1000)
        this.camera.position.set(0, 0, 2)
        this.controls = new OrbitControls(this.camera, 
            this.renderer.domElement)
        this.time = 0
        
        this.isPlaying = true
        
        this.addObjects()
        this.resize()
        this.render()
        this.setupResize()
        this.settings()
    }

    settings(){
        let that = this;
        this.settings = {
            progress: 0,
        }
        this.gui = new dat.GUI()
        this.gui.add(this.settings, "progress", 0, 1, 0.01)
    }

    setupResize() {
        window.addEventListener("resize", this.resize.
        bind(this))
    }

    resize() {
        this.width = this.container.offsetWidth
        this.height = this.container.offsetHeight
        this.renderer.setSize(this.width, this.height)
        this.camera.aspect = this.width / this.height

        this.imageAspect = 1688/2800
        let a1; let a2
        if(this.height/this.width > this.imageAspect){
            a1 = (this.width/this.height) * this.imageAspect
            a2 = 1
        } else {
            a1 = 1
            a2 = (this.height/this.width) / this.imageAspect
        }

        this.material.uniforms.resolution.value.x = this.width
        this.material.uniforms.resolution.value.y = this.height
        this.material.uniforms.resolution.value.z = a1
        this.material.uniforms.resolution.value.w = a2

        this.camera.updateProjectionMatrix()
    }

    addObjects() {
        let that = this
        this.material = new THREE.ShaderMaterial({
            extensions:{
                derivatives: "#extension GL_OES_standard_derivatives : enable"
            },
            side: THREE.DoubleSide,
            uniforms: {
                time: { value: 0 },
                progress: { value: 0 },
                t1: { type: "t", value: this.textures[0]},
                t2: { type: "t", value: this.textures[1]},
                resolution: { value: new THREE.Vector4() },
            },
            vertexShader: vertex,
            fragmentShader: fragment
        })

        this.geometry = new THREE.PlaneGeometry(1, 1,
            1,1)

        this.plane = new THREE.Mesh(this.geometry,
            this.material)
        this.scene.add(this.plane)
    }

    stop() {
        this.isPlaying = false
    }

    play() {
        if(!this.isPlaying){
            this.render()
            this.isPlaying = true
        }
    }

    render() {
        if(!this.isPlaying) return
        this.time += 0.05
        // console.log(this.time)
        this.material.uniforms.time.value = this.time
        this.material.uniforms.progress.value = this.settings.progress
        this.renderer.render(this.scene, this.camera)
        window.requestAnimationFrame(this.render.bind(this))
    }
}

new Sketch({
    dom: document.getElementById("container")
});
