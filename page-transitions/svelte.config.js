/** @type {import('@sveltejs/kit').Config} */

import adapter from '@sveltejs/adapter-static';
import dotenv from 'dotenv';
dotenv.config();

const config = {
	kit: {
		adapter: adapter({
			// default options are shown
			pages: 'build',
			assets: 'build',
			fallback: null
		}),
		paths: {
			base: `${process.env['BASE_PATH']}`
		}
	}
};

export default config;
